<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Route::controller('events/', 'EventController');
Route::resource('events', 'EventController');
Route::resource('gifts', 'GiftController'); //@deprecated
Route::resource('api/gifts', 'GiftController');
Route::resource('api/wishlists', 'WishlistController');
Route::resource('api/wishtoken', 'ReservationController');


Route::get('session', function() {
	$user = Facebook::session();
	if (!$user) {
		Log::error(__METHOD__ . ' failed to login');
		return Redirect::home()->withErrors('failed to login with Facebook');
	}
 	Auth::login($user);

	return Redirect::home();

});

Route::get('login', function() {
	echo 'you need to be logged in to access this data';
});

Route::get('logout', function() {
	Log::debug(__METHOD__ . ' user ['.(Auth::user() ? Auth::user()->id : '').'] is logging out');
	Auth::logout();
	return Redirect::home();
});

Route::get('/', array( 'as' => 'home',  function()
{
	return View::make('index');
}));

Route::get('/loggedin', function()
{
	return Auth::check() ? 1 : 0;
});

Route::get('/logout', function()
{
	Auth::logout();
	return Redirect::home();
});


//TODO: remove this
//todo: not needed
Route::get('/im/{id}', function($id) {
	if (getenv('ALLOW_FAKE_LOGIN'))
	{
		Auth::login(User::find($id));
	}

	return Redirect::home();
});