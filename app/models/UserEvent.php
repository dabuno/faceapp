<?php


class UserEvent extends Eloquent {

	protected $fillable = ['name', 'when', 'time', 'owner_id'];

	protected $hidden = array('created_at', 'updated_at', 'owner_id');


	public function gifts()
	{
		// return $this->hasMany('Gift', 'id', 'event_id');
		return $this->hasMany('Gift', 'event_id');
	}

}