<?php use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Wishlist extends Eloquent {

    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];

	protected $fillable = ['name', 'owner_id'];

	protected $hidden = array('deleted_at', 'created_at', 'updated_at', 'owner_id');


	public function gifts()
	{
		// return $this->hasMany('Gift', 'id', 'event_id');
		return $this->hasMany('Gift', 'wishlist_id');
	}

}