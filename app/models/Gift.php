<?php


class Gift extends Eloquent {

	protected $fillable = ['name', 'link', 'price', 'status', 'event_id', 'owner_id', 'wishlist_id'];

	protected $hidden = array('created_at', 'updated_at', 'owner_id');

}