<?php

// Blade::setContentTags('[%', '%]');

require_once app_path('helpers/js2php.php');
require_once app_path('helpers/debug.php');

/*
|--------------------------------------------------------------------------
| Facebook login/register to our website, events listeners
|--------------------------------------------------------------------------
|
| Here we add emails, so forth an so on...
*/
Event::listen('facebook.registered', function($user)
{
    Log::debug(__METHOD__ . ' user id ['.$user->id.'] registered');
});
Event::listen('facebook.loggedin', function($user)
{
    Log::debug(__METHOD__ . ' user id ['.$user->id.'] logged in');
});


App::missing(function($exception)
{
	return View::make('index');
});


App::error(function(Illuminate\Database\Eloquent\ModelNotFoundException $exception) {

    // Log the error
    Log::error($exception);

    //if we're on ajax, continue on ajax with the error
    if (Request::ajax()) {
	    return Response::json([
	    	'error' => true,
	    	'message' => 'An error occured. We\'re sorry, please try again later.'
		]);
    }

    Flash::error('An error occured. We\'re sorry, please try again later.');

    // Redirect to error route with any message
    return Redirect::home();


 //    return Response::json([
 //    	'error' => true,
 //    	'message' => 'An error has occured'
	// ]);
});


/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

	app_path().'/commands',
	app_path().'/controllers',
	app_path().'/models',
	app_path().'/database/seeds',

));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useFiles(storage_path().'/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::error(function(Exception $exception, $code)
{
	Log::error($exception);
});

/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
	return Response::make("Be right back!", 503);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/

require app_path().'/filters.php';
