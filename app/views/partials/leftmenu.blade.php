<div ng-view></div>

<div class="list-group">
  <a href="#" class="list-group-item active">
    Gifts
  </a>
  <a href="#" class="list-group-item ">
Wishlist <span class="badge">24</span>
</a>
<a href="#" class="list-group-item ">
Received <span class="badge">1</span>
</a>
</div>

<ul class="list-group">
  <li class="list-group-item active">Events <span class="badge">4</span><a href="{{action('EventController@create')}}" class="pull-right head-action">add</a></li>

  <li class="list-group-item"><a href="{{action('EventController@show', 1)}}">Wedding</a> <span class="badge">12</span><a href="#" class="pull-right body-action">enable</a><a href="#" class="pull-right body-action">disable</a><a href="#" class="pull-right body-action">share</a><a href="#" class="pull-right body-action">delete</a><a href="#" class="pull-right body-action">edit</a></li>

  <li class="list-group-item">Birthday <span class="badge">3</span><a href="#" class="pull-right body-action">enable</a></li>
  <li class="list-group-item">Event x <span class="badge">y</span><a href="#" class="pull-right body-action">enable</a></li>
</ul>


@section('scripts')
<script type="text/javascript">
	$(function() {
		$('body').on('mouseover', '.fa', function() {
			$(this).addClass('fa-lg');
		});
		$('body').on('mouseout', '.fa', function() {
			$(this).removeClass('fa-lg');
		});

	});

	//add tooltip
	$(function() {
	  	$('body').tooltip({
			selector: '[rel=tooltip]'
		});
	});
</script>
@stop