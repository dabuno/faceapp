@extends('master')
@section('content')

@include('partials.navigation')
<ol class="breadcrumb">
  <li><a href="{{url('/')}}">Home</a></li>
  <li><a href="{{action('EventController@index')}}">Events</a></li>
  <li class="active">{{{ $event->name }}}</li>
</ol>

{{ Form::open(['url' => 'events/'.$event->id, 'method' =>'put', 'class'=>'form', 'role'=>'form']) }}
@include('events.createoredit')
{{ Form::close() }}

@stop

