@extends('master')
@section('content')
@include('partials.navigation')

<ol class="breadcrumb">
  <li><a href="{{url('/')}}">Home</a></li>
  <li><a href="{{url('events')}}">Events</a></li>
  <li class="active">Add</li>
</ol>

{{ Form::open(['url' => 'events', 'method' =>'post', 'class'=>'form', 'role'=>'form']) }}
	@include('events.createoredit')
{{ Form::close() }}

@stop