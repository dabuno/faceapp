@include('master')

@section('content')

<ol class="breadcrumb">
  <li><a href="{{url('/')}}">Home</a></li>
  <li><a href="{{url('events')}}">Events</a></li>
  <li class="active">Add</li>
</ol>

<i class="glyphicon glyphicon-search"></i>

<div>**** Event properties:</div>
<div>  - [ ] name(predefined, or custom name), (required)</div>
<div>  - [ ] date(required), (recurring)</div>
<div>  - [ ] time(optional), (recurring)</div>
<div>  - [ ] gifts - see Event Gifts</div>
<div>  - [ ] save and share later / save and share with friends</div>

<a class="btn btn-primary" href="#" role="button">Save</a>
<a class="btn btn-default" href="#" role="button">Cancel</a>

@show