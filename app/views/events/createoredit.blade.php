@if(count($errors))
	<div role='alert' class="alert alert-danger">
		{{ HTML::ul($errors->all()) }}
	</div>
	@endif
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<!-- `Name` Field -->
				{{ Form::label('name', 'Name') }}
				{{ Form::text('name', Input::old('name') ?: $event->name , ['class'=>'form-control input-sm', 'placeholder' => 'event name like: Birthdate, Wedding']) }}
			</div>

			<div class="row">
  				<div class="col-xs-6">
				<!-- `Day` Field -->
				{{ Form::label('when', 'On') }}
				{{ Form::text('when', Input::old('when') ?: date2js($event->when), ['class'=>'form-control input-sm', 'id' => 'datepicker']) }}
				</div>

				<div class="col-xs-6">
				<!-- `Time` Field -->
				{{ Form::label('time', 'At') }}
				{{ Form::text('time', Input::old('time') ?: time2js($event->time), ['class'=>'form-control input-sm ', 'id' => 'timepicker']) }}
				</div>
			</div>
			<div style="margin-top:20px">
				<!-- Form actions -->
				<button type='submit' class="btn btn-primary btn-xs">Save</button>
				<a href='{{URL::previous()}}' class="btn btn-default btn-default-decolor btn-xs">Cancel</a>
			</div>
		</div>
	</div>

@section('scripts')
<script type="text/javascript">
// Datepicker
$(function() {
	$('#datepicker').datepicker({
	    inline: true,
	    dateFormat: 'd M, yy'
	});

	$('#timepicker').timepicker({
		stepMinute: 10,
		hourGrid: 4,
		minuteGrid: 10,
		timeFormat: 'HH:mm',
		hour: {{hour2js($event->time)}},
		minute: {{minute2js($event->time)}}
	});
})
</script>
@stop