@extends('master')
@section('content')
@include('partials.navigation')
<ol class="breadcrumb">
  <li><a href="{{url('/')}}">Home</a></li>
  <li class="active">Events</li>
</ol>

@if (!count($events))
	No events defined. <a href="{{action('EventController@create')}}">add event</a>
@endif

<div class="row">
	<div class="col-md-12">
		<table class="table table-hover">
			<tr>
				<th>Name</th>
				<th>On</th>
				<th>At</th>
				<th>&nbsp;</th>
			</tr>
			@foreach($events as $event)
			<tr>
				<td>{{{ @$event->name }}}</td>
				<td>{{ date2js($event->when)}}</td>
				<td>{{ time2js($event->time) }}</td>
				<td>
					<a href='events/{{$event->id}}/edit' class="btn btn-primary btn-xs">Edit</a>
					{{ Form::open(['url'=>'events/'.$event->id, 'method'=>'delete', 'style' => 'width:100px; display:inline']) }}
						<button type='submit' class="btn btn-default btn-default-decolor btn-xs">Delete</button>
					{{ Form::close() }}
				</td>
			</tr>
			@endforeach
		</table>
	</div>
</div>

@stop