@extends('master')

{{-- Web site Title --}}
@section('title')
@parent
:: Home
@stop

@section('content')

@include('partials.navigation')
{{-- @include('partials.leftmenu') --}}

@if (Auth::check())
	<div ng-view></div>
@endif

<div class="jumbotron">
	<h1>{{Config::get('site.name')}}</h1>
	<p>Simple app for gifts management.</p>
	It takes 3 easy steps: create an event, add gifts and share it with your friends. They'll know what to do.
    <p style="color:red">add with scrolldown a preview for each step</p>
    @if (Auth::guest())
    <p>
	<i class="fa fa-facebook-square fa-lg" style="color:#3b5998; background:#fff"></i>
		{{Facebook::loginURL()}} to see more!
	</p>
	@endif

</div>

@stop