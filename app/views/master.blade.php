<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            @section('title')
            {{Config::get('site.name')}}
            @show
        </title>
        <meta name="description" content="Simple app for gifts management. It's free.">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @if (getenv('USE_CDNJS'))
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
        @else
        {{ HTML::style('assets/css/bootstrap.min.css'); }}
        {{ HTML::style('assets/css/font-awesome.min.css'); }}
        @endif
        {{ HTML::style('css/main.css'); }}


        <!-- Images -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('assets/images/apple-touch-icon-144-precomposed.png') }}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('assets/images/apple-touch-icon-114-precomposed.png') }}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('assets/images/apple-touch-icon-72-precomposed.png') }}">
        <link rel="apple-touch-icon-precomposed" href="{{ asset('assets/images/apple-touch-icon-57-precomposed.png') }}">

        <!-- ICO -->
        <link rel="shortcut icon" href="favicon.ico">
        <base href="{{getenv('JS_BASE')}}">

        <script>
            var gaio = {
                __appId: '{{getenv('FACEBOOK_APPID')}}'
            };
        </script>

<meta property="og:url"                content="http://faceapp.com/wishlists/62/" />
<meta property="fb:app_id"             content="{{getenv('FACEBOOK_APPID')}}" />
<meta property="og:type"               content="article" />
<meta property="og:title"              content="auch wishlist" />
<meta property="og:description"        content="xulescu ce-si doreste?" />

    </head>

    <body ng-app="giftapp" ng-init="__base_url='{{url()}}';__base='{{getenv('JS_BASE')}}';__appId='{{getenv('FACEBOOK_APPID')}}'">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=331557063696640";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <!-- Container -->
        <div class="container">
            @include('flash::message')
            <!-- Notifications -->
            {{-- @include('partials.notifications') --}}
            <!-- notifications -->




            <!-- Content -->
            @yield('content')
            <!-- ./ content -->
        </div>
        <!-- ./ container -->

        <!-- JS -->
        <!-- jQuery -->
        @if (getenv('USE_CDNJS'))
	        <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
	        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.10/angular.min.js"></script>
	        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.11/angular-route.min.js"></script>
	        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.10/angular-resource.min.js"></script>
            <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

        @else
	        {{Html::script('assets/dev/jquery-1.11.1.js')}}
	        {{Html::script('assets/dev/angular.js')}}
	        {{Html::script('assets/dev/angular-route.js')}}
            {{Html::script('assets/dev/angular-resource.js')}}
	        {{Html::script('assets/js/bootstrap.min.js')}}
        @endif


        <script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>

        <script src="https://code.jquery.com/ui/1.11.2/jquery-ui.min.js"></script>
        {{Html::style('css/custom-theme/jquery-ui-1.10.3.custom.css')}}
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.4.5/jquery-ui-timepicker-addon.min.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.4.5/jquery-ui-timepicker-addon.min.js"></script>

        <!-- <script src="app.js"></script> -->
{{--
        {{Html::script('assets/js/app.js')}}
        {{Html::script('assets/js/controllers.js')}}
--}}
        {{Html::script('assets/js/dist/app.min.js')}}

        <!--
        <script src="{{ asset('assets/scripts/js/plugins.js') }}"></script>
        <script src="{{ asset('assets/scripts/js/main.js') }}"></script>
        -->
        @yield('scripts')
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            // var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
            // (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            // g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            // s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
    </body>
</html>