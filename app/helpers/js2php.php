<?php



if ( ! function_exists('date2php'))
{
   function date2php($jsdate) {
      return date('Y-m-d', strtotime($jsdate));
   }
}

if ( ! function_exists('dmy2js'))
{
   function dmy2js($jsdate) {
      //global vsjCalendar::$DAYS, vsjCalendar::$MONTHS;
      return date('d.m.Y', strtotime($jsdate));
   }
}

if ( ! function_exists('date_long_2js'))
{
   function date_long_2js($jsdate) {
      //global vsjCalendar::$DAYS, vsjCalendar::$MONTHS;
      $jsdate = date("l, j F Y", strtotime($jsdate));
      return $jsdate; // afiseaza Luni, 30 Mai 2005
   }
}

if ( ! function_exists('date2js'))
{
   function date2js($jsdate) {
      //global vsjCalendar::$DAYS, vsjCalendar::$MONTHS;
      $jsdate = date("j M, Y", strtotime($jsdate));
      return $jsdate; // afiseaza Luni, 30 Mai 2005
   }
}

if ( ! function_exists('time2js'))
{
   function time2js($time) {
      return date('H:i', strtotime($time));
   }
}


if ( ! function_exists('hour2js'))
{
   function hour2js($time) {
      return date('H', strtotime($time));
   }
}
if ( ! function_exists('minute2js'))
{
   function minute2js($time) {
      return date('i', strtotime($time));
   }
}