<?php

function ddq() {
	$queries = DB::getQueryLog();
	print_r(end($queries));
	die();
}


if (!function_exists('prin')) {
function prin($vars,$die=0,$caption='', $flush = true) {
   //return false;
   /*
   if ($_SERVER["REMOTE_ADDR"] != '10.21.2.20' && $_SERVER["REMOTE_ADDR"] != '10.21.8.20'  && $_SERVER["REMOTE_ADDR"] != '10.21.3.9' && $_SERVER["REMOTE_ADDR"] != '95.76.160.48' )
   return false;
   */
   echo '<div style="background:white;text-align:left">';
   if (@disable_prin == 1)
     return false;
   if ($die>=0) {
      if ( is_array($vars) || is_object($vars) ) {
         echo (is_string($die)?$die:'').$caption.'<b style="color:red">:: '.(is_array($vars)?'array':'object').' ::</b><pre><span style="color:blue">';
         print_r($vars);
         echo '</pre>';
      } else {
         echo (is_string($die)?$die:'').$caption.'<b style="color:red">:: '.('variable').' ::</b><pre><span style="color:blue">';
         print_r($vars);
         echo '</pre>';
      }
   }

   if ($die<0) {
      if ( is_array($vars) || is_object($vars) ) {
         echo (is_string($die)?$die:'').$caption.'<b style="color:red">:: '.(is_array($vars)?'array':'object').' ::</b><pre><span style="color:blue">';
         var_dump($vars);
         echo '</pre>';
      } else {
         echo (is_string($die)?$die:'').$caption.'<b style="color:red">:: '.('variable').' ::</b><pre><span style="color:blue">';
         var_dump($vars);
         echo '</pre>';
      }
   }

   if ($flush)
   {
      echo str_pad(' ', 1024*8);
      flush();
      //sleep($flush);
   }

   if ($die == 1 || $die=='-1')
      die("<b>:: died ::</b>");
   echo '</div>';
}
}