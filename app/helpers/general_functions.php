<?php //namespace Dabuno\Helpers;
if ( ! function_exists('vd'))
{
	/**
	 * Dump the passed variables and end the script.
	 *
	 * @param  mixed
	 * @return void
	 */
	function vd()
	{
		array_map(function($x) { var_dump($x); }, func_get_args());
	}
}