<?php

class EventController extends \BaseController {

    public function __construct()
    {
        $this->beforeFilter('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// return UserEvent::whereOwnerId(Auth::user()->id)->with('gifts')->get();
		$events = UserEvent::whereOwnerId(Auth::user()->id)->with(['gifts' => function($query) {
			$query->whereStatus('wishlist');
		}])->get();
		// foreach ($events as &$event) {
		// 	$event->no_wishlist = $event->gifts()->whereStatus('wishlist')->count();
		// }

		return $events;
		// $ev = UserEvent::whereOwnerId(Auth::user()->id)->with('gifts')->first();
		// ddq();
		// dd($events[0]->no_wishlist);


		// $events = UserEvent::all();
		// $events = GlobalEvent::all();

		// return View::make('events.list')->with('events', $events);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $event = new UserEvent;
        $event->when = date('Y-m-d');//, strtotime('now + 1 month'));
        $event->time = '18:00';//, strtotime('now + 1 month'));
		$event->owner_id = Auth::user()->id;
		return View::make('events.create')->with('event', $event);
	}

    /**
     * Store a newly created event in storage.
     *
     * @return Response
     */
    public function store()
    {
    	//,email_address,NULL,id,account_id,1'
    	$owner_id = Auth::user()->id;
        $validator = Validator::make($data = Input::all(), [
        	'name' => 'required|unique:user_events,name,NULL,owner_id,owner_id,' . $owner_id]);


        if ($validator->fails())
        {
            return Response::json(['error' => true, 'message' => $validator->getMessageBag()], 422);
        }

        if (Input::get('when'))
        {
	        $data['when'] = date2php(Input::get('when'));
        }
        $data['owner_id'] = $owner_id;
        $event = UserEvent::create($data);

        return $event;
        // return Redirect::route('events.index');
    }

    /**
     * Display the specified event.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
    	return UserEvent::whereId($id)->whereOwnerId(Auth::user()->id)->with(['gifts' => function($query) {
			$query->whereStatus('wishlist');
		}])->firstOrFail();
        // $event = UserEvent::findOrFail($id);

        // return View::make('events.show', compact('event'));
    }

    /**
     * Show the form for editing the specified event.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
    	return UserEvent::whereId($id)->whereOwnerId(Auth::user()->id)->firstOrFail();

        return View::make('events.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
    	$event = UserEvent::whereId($id)->whereOwnerId(Auth::user()->id)->firstOrFail();

        $validator = Validator::make($data = Input::all(), ['name' => 'required|unique:user_events,name,'.$id.',id,owner_id,' . Auth::user()->id]);
        if ($validator->fails())
        {

        	    //return Redirect::back()->withErrors($validator)->withInput();
            return Response::json(['error' => true, 'message' => $validator->getMessageBag()], 422);
        }

        if (Input::get('when'))
        {
        	$data['when'] = date2php(Input::get('when'));
    	}
        $event->update($data);

        return $event;
        // return Redirect::route('events.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
    	$event = UserEvent::whereId($id)->whereOwnerId(Auth::user()->id)->firstOrFail();

        return UserEvent::destroy($id);

        // return Redirect::route('events.index');
    }

}
