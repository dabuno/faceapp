<?php

class WishlistController extends \BaseController {

    public function __construct()
    {
        $this->beforeFilter('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$wishlists = Wishlist::whereOwnerId(Auth::user()->id)->with(
            array('gifts' => function($q) {
                $q->whereStatus('wishlist');
            }))->get();
		// foreach ($wishlists as &$event) {
		// 	$event->no_wishlist = $event->gifts()->whereStatus('wishlist')->count();
		// }

		return $wishlists;
		// $ev = UserEvent::whereOwnerId(Auth::user()->id)->with('gifts')->first();
		// ddq();
		// dd($events[0]->no_wishlist);


		// $events = UserEvent::all();
		// $events = GlobalEvent::all();

		// return View::make('events.list')->with('events', $events);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

	}

    /**
     * Store a newly created event in storage.
     *
     * @return Response
     */
    public function store()
    {
    	//,email_address,NULL,id,account_id,1'
    	$owner_id = Auth::user()->id;
        $data = Input::all();
        $data['owner_id'] = $owner_id;
        $validator = Validator::make($data, [
            // 'name' => 'required|unique:wishlists,name,NULL,owner_id,owner_id,' . $owner_id,
        	'name' => 'required',
            'owner_id' => 'required'
        ]);


        if ($validator->fails())
        {
            return Response::json(['error' => true, 'message' => $validator->getMessageBag()], 422);
        }

        $wishlist = Wishlist::create($data);
        $wishlist->token = str_random(10);
        if ( ! $wishlist->save()) {
            Log::error(__METHOD__ . ' failed to generate unique token ['. $wishlist->token .']');
            return Response::json(['error' => true, 'message' => 'An error occured. Please try again.'], 422);
        }

        return Wishlist::whereId($wishlist->id)->whereOwnerId(Auth::user()->id)->with(['gifts' => function($query) {
            $query->whereStatus('wishlist');
        }])->firstOrFail();

    }

    /**
     * Display the specified event.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
    	return Wishlist::whereId($id)->whereOwnerId(Auth::user()->id)->with(['gifts' => function($query) {
			$query->whereStatus('wishlist');
		}])->first();//->firstOrFail();
        // $event = UserEvent::findOrFail($id);

        // return View::make('events.show', compact('event'));
    }

    /**
     * Show the form for editing the specified event.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
    	$wishlist = Wishlist::whereId($id)
            ->whereOwnerId(Auth::user()->id)
            ->with(['gifts' => function($query) {
                $query->whereStatus('wishlist');
            }])
            ->firstOrFail();

        $validator = Validator::make($data = Input::all(), [
            // 'name' => 'required|unique:wishlists,name,'.$id.',id,owner_id,' . Auth::user()->id]);
            'name' => 'required'
        ]);
        if ($validator->fails())
        {

        	    //return Redirect::back()->withErrors($validator)->withInput();
            return Response::json(['error' => true, 'message' => $validator->getMessageBag()], 422);
        }

        $wishlist->update($data);

        return $wishlist;
        // return Redirect::route('events.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
    	$wishlist = Wishlist::whereId($id)->whereOwnerId(Auth::user()->id)->firstOrFail();
		return Wishlist::destroy($id);
        // return Redirect::route('events.index');
    }

}
