<?php

class GiftController extends \BaseController {

	/**
     * Instantiate a new UserController instance.
     */
    public function __construct()
    {
        $this->beforeFilter('auth');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Gift::whereOwnerId(Auth::user()->id)->get();
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$owner_id = Auth::user()->id;
        $validator = Validator::make($data = Input::all(), [
        	// 'name' => 'required|unique:gifts,name,NULL,owner_id,owner_id,' . $owner_id
        	'name' => 'required'
    	]);

        if ($validator->fails()) {
            return Response::json(['error' => true, 'message' => $validator->getMessageBag()], 422);
        }

        $data['owner_id'] = $owner_id;
        $gift = Gift::create($data);

        return $gift;
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
    	$gift = Gift::whereId($id)->whereOwnerId(Auth::user()->id)->firstOrFail();

        $validator = Validator::make($data = Input::all(), [
        	// 'name' => 'required|unique:gifts,name,'.$id.',id,owner_id,' . Auth::user()->id
        	'name' => 'required'
    	]);
        if ($validator->fails())
        {
            return Response::json(['error' => true, 'message' => $validator->getMessageBag()], 422);
        }

		$gift->update($data);

        return $gift;
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
    	$gift = Gift::whereId($id)->whereOwnerId(Auth::user()->id)->firstOrFail();
        return Gift::destroy($id);

	}



}
