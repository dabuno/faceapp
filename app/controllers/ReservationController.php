<?php

class ReservationController extends \BaseController {

	public function __construct()
	{
		$this->beforeFilter('auth');
	}

	public function __call($arg1, $arg2) {
		Log::error(__METHOD__ . ' invalid method ['.$arg1.']');
		if (Request::ajax()) {
			return Response::json(['error' => true, 'message' => 'An error occured. We\'re sorry. Please try again later.'], 422);
		}

		return Redirect::home();
	}


	/**
	 * Display the specified event.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$wishlist = Wishlist::whereToken($id)->with(['gifts' => function($query) {
			$query->whereStatus('wishlist');
		}])->firstOrFail();

		if ($wishlist->owner_id == Auth::user()->id) {
			$wishlist->redirect = true;
			unset($wishlist->gifts);
			return $wishlist;
		}

		return $wishlist;
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$wishlist = Wishlist::whereId($id)
			->whereOwnerId(Auth::user()->id)
			->with(['gifts' => function($query) {
				$query->whereStatus('wishlist');
			}])
			->firstOrFail();

		$validator = Validator::make($data = Input::all(), [
			// 'name' => 'required|unique:wishlists,name,'.$id.',id,owner_id,' . Auth::user()->id]);
			'name' => 'required'
		]);
		if ($validator->fails())
		{
			return Response::json(['error' => true, 'message' => $validator->getMessageBag()], 422);
		}

		$wishlist->update($data);

		return $wishlist;
		// return Redirect::route('events.index');
	}
}
