<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsGlobalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('global_events', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 250);
			$table->string('custom_name', 250);
			$table->tinyInteger('day')->nullable();
			$table->tinyInteger('month')->nullable();
			$table->smallInteger('year')->nullable();
			$table->time('time')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('global_events');
	}

}
