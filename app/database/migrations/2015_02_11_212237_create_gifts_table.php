<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGiftsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('gifts', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->integer('owner_id');
			$table->string('name', 250);
			$table->string('link', 500)->nullable();
			$table->integer('price')->nullable();
			$table->enum('status', array('wishlist', 'received'))->default('wishlist');
			//gift controller should aggregate.
			$table->index('owner_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gifts');
	}

}
