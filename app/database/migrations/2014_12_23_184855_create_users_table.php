<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table)
		{
		    $table->bigIncrements('id');
		    $table->bigInteger('fbid');
		    $table->string('email', 250)->nullable();
		    $table->string('name', 500)->nullable();
		    $table->string('first_name', 250)->nullable();
		    $table->string('last_name', 250)->nullable();
		    $table->string('bday', 250)->nullable();
		    $table->string('gender', 10)->nullable();
		    $table->string('fb_link', 500);
		    $table->string('locale', 50)->nullable();
		    $table->integer('timezone')->nullable();
		    $table->dateTime('fb_updated_time')->nullable();
		    $table->boolean('verified')->nullable();


		    $table->string('fb_access_token', 500)->nullable();
		    $table->dateTime('fb_access_expires_at')->nullable();

			// 'id' => string '1381425015483481' (length=16)
			// 'email' => string 'ubikandrew@gmail.com' (length=20)
			// 'first_name' => string 'Ubik' (length=4)
			// 'gender' => string 'male' (length=4)
			// 'last_name' => string 'Andrew' (length=6)
			// 'link' => string 'https://www.facebook.com/app_scoped_user_id/1381425015483481/' (length=61)
			// 'locale' => string 'en_US' (length=5)
			// 'name' => string 'Ubik Andrew' (length=11)
			// 'timezone' => int 2
			// 'updated_time' => string '2014-11-29T22:48:18+0000' (length=24)
			// 'verified' => boolean true

		    //laravel specifics
		    $table->softDeletes();
		    $table->timestamps();	// Adds created_at and updated_at columns
		    $table->rememberToken();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}

}
