<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGlobalEventsRows extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		// Insert some stuff

	    DB::table('global_events')->insert(array(
	    array(
	        'name' => 'Birthday',
		    'day' => '',
	        'month' => ''
	    ),
	    array(
	        'name' => 'Wedding',
		    'day' => '',
	        'month' => ''
	    ),
	    array(
	        'name' => 'Christmas',
	        'day' => '25',
	        'month' => '12'
	    ),

	    array(
	        'name' => 'Valentine\'s',
	        'day' => '14',
	        'month' => '2'
	    ),
	    array(
	        'name' => 'Newborn',
		    'day' => '',
	        'month' => ''

	    ),
	    array(
	        'name' => 'Baptism',
		    'day' => '',
	        'month' => ''

	    ),
	));
		//
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('global_events')->truncate();
	}

}
