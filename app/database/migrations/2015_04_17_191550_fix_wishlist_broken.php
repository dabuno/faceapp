<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixWishlistBroken extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('gifts', function($table) {
			$table->integer('wishlist_id');
			$table->index('wishlist_id');
			// $table->dropColumn('event_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('gifts', function($table) {
			$table->dropColumn('wishlist_id');
		});
	}


}
