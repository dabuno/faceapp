<?php namespace Dabuno\Facebook;

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\GraphUser;

use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Event;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Log\Writer;
use \Log;
use \User;



class Facebook {

	protected $returnURL;

	public function __construct(UrlGenerator $url, $returnURL = '')
	{
		if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}

		$this->returnURL = $returnURL ?: $url->to('session');

		FacebookSession::setDefaultApplication(getenv('FACEBOOK_APPID'), getenv('FACEBOOK_APPSECRET'));
	}


	public function FunctionName($value='')
	{
		return 'Testing';
	}

	public function session()
	{

		$session = null;
		$helper = new FacebookRedirectLoginHelper($this->returnURL);
		try {
		    $session = $helper->getSessionFromRedirect();
		} catch(FacebookRequestException $ex) {
		    // When Facebook returns an error
		} catch(\Exception $ex) {
		    // When validation fails or other local issues
		}

		if (!$session)
		{
			Log::error(__METHOD__ . ' failed to get facebook session');
			return false;
		}


		$user_profile = (new FacebookRequest(
      		$session, 'GET', '/me'
	    ))->execute()->getGraphObject(GraphUser::className());


		$fbid = $user_profile->getId();
		if ( ! $fbid )
		{
			Log::error(__METHOD__ . ' failed to get fbid ['.$fbid.']');
			return false;
		}

		//check against our db. see if we register it or log in the user
		$user = User::whereFbid($fbid)->first();
		if ( ! $user_found = count($user) )
		{
			$user = new User;
		}

		//facebook to our db
		$user->email                = $user_profile->getEmail();
		$user->name                 = $user_profile->getName();
		$user->bday                 = $user_profile->getBirthday();
		$user->fbid                 = $user_profile->getId();
		$user->fb_link              = $user_profile->getLink();

		$user->gender               = $user_profile->getProperty('gender');
		$user->locale               = $user_profile->getProperty('locale');
		$user->timezone             = $user_profile->getProperty('timezone');
		$user->fb_updated_time      = $user_profile->getProperty('fb_updated_time');
		$user->verified             = $user_profile->getProperty('verified');

		$user->fb_access_token      = $session->getToken();
		$user->fb_access_expires_at = $session->getAccessToken()->getExpiresAt();

		//update db last login
		if ( ! $user->save() )
		{
			Log::error(__METHOD__ . ' failed to register / login fbid ['.$fbid.']');
			$response = Event::fire('facebook.error', array($user));
			return false;
		}

		if ($user_found)
		{
			$response = Event::fire('facebook.loggedin', array($user));
		}
		else
		{
			$response = Event::fire('facebook.registered', array($user));
		}


		return $user;



		/**
		 * we are here
		 *
		 * processing facebook retrieved data
		 *
		 * 1. we need to login user, and match its data in our website.
		 * probably using facebook user id
		 *
		 * 2. update its profile details if needed
		 *
		 * 3. create Auth::user, maybe using Auth::login or something like this. check laracasts bdd episode
		 *
		 * 4. create a test for facebook test user, to see if login works
		 *
		 * 5. access token revalidation
		 *
		 *
		 */
		/*
Name: Ubik AndrewgetBirthday:
object(Facebook\GraphSessionInfo)[177]
  protected 'backingData' =>
    array (size=7)
      'app_id' => string '331557063696640' (length=15)
      'application' => string 'idealis' (length=7)
      'expires_at' => int 1424210433
      'is_valid' => boolean true
      'issued_at' => int 1419026433
      'scopes' =>
        array (size=3)
          0 => string 'public_profile' (length=14)
          1 => string 'email' (length=5)
          2 => string 'read_friendlists' (length=16)
      'user_id' => string '1381425015483481' (length=16)
object(Facebook\FacebookSession)[171]
  private 'accessToken' =>
    object(Facebook\Entities\AccessToken)[159]
      protected 'accessToken' => string 'CAAEtjKUBIQABANwrbAewemx7x1ZCDv5aSuGzS4OXuAXCswrUCrdSyUSG4WSxnXUhz98LiQSGFxNNuPrYCjEzLeeuuZBEzQG1DWdcn1ip6IU70w0eY3pZCZBPSz41yACf85JpNQKVsOdnIe5oFaHJHrMsxgTjWYwZC4ZB1ZAVAOjWZB0j3ZBP4eT2PWmy8rOXVSDsEX59Vg2K58ScXQNumgQ1ZB' (length=219)
      protected 'machineId' => null
      protected 'expiresAt' =>
        object(DateTime)[176]
          public 'date' => string '2015-02-17 22:00:33' (length=19)
          public 'timezone_type' => int 3
          public 'timezone' => string 'UTC' (length=3)
  private 'signedRequest' => null
object(Facebook\GraphUser)[173]
  protected 'backingData' =>
    array (size=11)
      'id' => string '1381425015483481' (length=16)
      'email' => string 'ubikandrew@gmail.com' (length=20)
      'first_name' => string 'Ubik' (length=4)
      'gender' => string 'male' (length=4)
      'last_name' => string 'Andrew' (length=6)
      'link' => string 'https://www.facebook.com/app_scoped_user_id/1381425015483481/' (length=61)
      'locale' => string 'en_US' (length=5)
      'name' => string 'Ubik Andrew' (length=11)
      'timezone' => int 2
      'updated_time' => string '2014-11-29T22:48:18+0000' (length=24)
      'verified' => boolean true
object(Facebook\FacebookRedirectLoginHelper)[166]
  private 'appId' => string '331557063696640' (length=15)
  private 'appSecret' => string '30e07969edb02a72027a3be538c7d270' (length=32)
  private 'redirectUrl' => string 'http://localhost/faceapp/faceredirect' (length=37)
  private 'sessionPrefix' => string 'FBRLH_' (length=6)
  protected 'state' => string '5eb9b45897dd42a2ea588b5c5bf8388e' (length=32)
  protected 'checkForSessionStatus' => boolean true
array (size=2)
  'code' => string 'AQCBFwhGH3DTTwDQ8-ctlIEZoPPRFEeIklLnF6psznLp9JHQCWrU0zGvSczwK0niZqvWyv1BSczfaEIDh89FxNrDVdbQIu-o5F2h0e-G6rW1CfaobdLo6CPPhYURBS-AaoqX-JcjLYJiqHOqd-A0gj4t6eVT2c0DAHpX6vI8ozH2wNE2fP1YkQJ5Yyq_CMVb9anIu4QPN1qHYJua9IhCSteVCH_bvV5jygTmEOOLDvowvRbDthZfuaDE6oNuHTAwHYMVHnj0R8IHO0w_ex3FwIh_7ZOlrhgYqeXjLdywBrvA0UhkNOB3Ku6SW_H0TQivHbyvWmeWZXghAJ-NLbR197zr' (length=344)
  'state' => string '5eb9b45897dd42a2ea588b5c5bf8388e' (length=32)
 */

		 //    echo "Name: " . $user_profile->getName();
		 //    echo "getBirthday: " . $user_profile->getBirthday();

		 //    // SessionInfo example
			// $info = $session->getSessionInfo();
			// var_dump($info);

		    // try {

		    //     $response = (new FacebookRequest(
		    //       $session, 'POST', '/me/feed', array(
		    //         'link' => 'www.facemeup.com',
		    //         'message' => 'X provided message'
		    //       )
		    //     ))->execute()->getGraphObject();

		    //     echo "Posted with id: " . $response->getProperty('id');

		    //   } catch(FacebookRequestException $e) {

		    //     echo "Exception occured, code: " . $e->getCode();
		    //     echo " with message: " . $e->getMessage();

		    //   }

			// var_dump($session);
			// var_dump($user_profile);
			// var_dump($helper);

		  // Logged in.
	}


	public function loginURL()
	{
		$helper = new FacebookRedirectLoginHelper($this->returnURL);
		return '<a href="' .
			$helper->getLoginUrl(array('scope' => 'public_profile')) . '">Login with Facebook</a>';
	}


	public function validate() {

	}
}