<?php namespace Dabuno\Facebook;

use Illuminate\Support\ServiceProvider;


class FacebookServiceProvider extends ServiceProvider {

	/**
	 * A deferred service provider is only loaded and registered 
	 * when one of the services it provides is actually needed 
	 * by the application IoC container.
	 * @var boolean
	 */
	protected $defer = true;

	
	public function register()
	{
		$this->app->bind('facebook', 'Dabuno\Facebook\Facebook');
	}

	/**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->package('dabuno/facebook', null, 'app/Dabuno/');
    }

	/**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['facebook'];
    }

}