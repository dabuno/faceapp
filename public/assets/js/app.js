(function() {
	var app = angular.module('giftapp', ['ngRoute','ngResource', 'controllers']);


	app.service('checkLoggedin', ['$q', '$timeout', '$http', '$location', '$rootScope', '$window',
		function($q, $timeout, $http, $location, $rootScope, $window) {
			return {
				isLogged: function()
				{
					var deferred = $q.defer();

					$http.get( $rootScope.__base + 'loggedin').success(function(user) {
						if (user !== '0') {
							deferred.resolve();
						}

						else {
							$rootScope.message = 'You need to log in.';
							deferred.reject();
							$window.location.href = $rootScope.__base + 'login';
						}

						return deferred.promise;
					});

					return deferred.promise;
				}
			}
		}]);

	app.config(['$routeProvider', '$locationProvider', '$interpolateProvider',
		function($routeProvider, $locationProvider, $interpolateProvider) {

			$interpolateProvider.startSymbol('[[');
			$interpolateProvider.endSymbol(']]');

        	$routeProvider.
	        	when('/wishlists/new', {
	        		templateUrl: 'html/wishlist.html',
	            	controller: 'WishlistController'
	        	}).

	        	when('/wishlists/:id/', {
	        		templateUrl: 'html/wishlist.html',
	            	controller: 'WishlistController'
	        	}).

	        	when('/wish/:id/', {
	        		templateUrl: 'html/wishbook.html',
	            	controller: 'WishlistBook'
	        	}).


				when('/received', {
	        		templateUrl: 'html/gift-list.html',
	            	controller: 'GiftReceivedController'
	        	}).

	        	when('/gifts/:giftId', {
	        		templateUrl: 'html/gift-detail.html',
	            	controller: 'GiftEditController'
	        	}).


	        	when('/events/:eventId/gifts/', {
	        		templateUrl: 'html/gift-detail.html',
	            	controller: 'GiftAddController'
	            }).

		    	when('/gifts/new', {
	            	templateUrl: 'html/gift-detail.html',
	            	controller: 'GiftAddController'
	         	}).

	          	when('/events/new', {
	            	templateUrl: 'html/event-detail.html',
	            	controller: 'EventAddController'
	          	}).

		        when('/events/:eventId', {
		            templateUrl: 'html/event-detail.html',
		            controller: 'EventDetailController'
		        }).

		        when('/logout', {
		        	template: '',
		            controller: 'UserController'
		        }).

		        when('/', {
		          	templateUrl: 'html/wishlists.html',
		          	resolve: {
		          		loggedin: ['checkLoggedin', function(checkLoggedin) {
		          			return checkLoggedin.isLogged();
		          		}]
	          		}
		        }).

		        otherwise({
		            redirectTo: '/'
		        });

	          $locationProvider.html5Mode(true);

	}]);


	app.directive('focusMe',
		function($timeout) {
		  return {
		    link: function(scope, element, attrs) {
		      scope.$watch(attrs.focusMe, function(value) {
		        if(value === true) {
		            $timeout(function() {
		            	element[0].focus();
		            });
		            scope[attrs.focusMe] = false;
		        }
		      });
		    }
		  };
		});

	app.directive('focusForm',
		function($timeout) {
		  return {
		    link: function(scope, element, attrs) {
		      scope.$watch(attrs.focusForm, function(value) {
		        if(value === true) {
		            $timeout(function() {
		            	element[0].focus();
		            });
		            scope[attrs.focusForm] = false;
		        }
		      });
		    }
		  };
		});


	app.directive('gaioDate', ['$filter',
		function($filter) {
		    return {
		        restrict: 'A',
		        require: 'ngModel',
		        link: function(scope, element, attr, ngModel) {
		            function toUser(text) {
		            	if (!text) {
		            		return '';
		            	}
		                return $filter('date')(text, 'mediumDate') || '';
		            }

		            ngModel.$formatters.push(toUser);
		        }
		    };
	}]);

	app.directive('gaioTime', ['$filter',
		function($filter) {
		    return {
		        restrict: 'A',
		        require: 'ngModel',
		        link: function(scope, element, attr, ngModel) {
		            function toUser(text) {
		            	if (!text) {
		            		return '';
		            	}
		            	var hms = text.split(':');
		            	var h = hms[0];
		            	var m = hms[1];

		                return $filter('date')(h+':'+m, 'HH:mm') || '';
		            }
		            // ngModel.$parsers.push(fromUser);
		            ngModel.$formatters.push(toUser);
		        }
		    };
	}]);

	app.factory('Wishlist', ['$resource',
		function($resource) {
		  	return $resource(
		  		'api/wishlists/:id',
		  		{id: '@id'},
		  		{
			        'update': { method:'PUT' }
		    	}
		    );
		}]);

	app.factory('WishlistToken', ['$resource',
		function($resource) {
		  	return $resource(
		  		'api/wishtoken/:id',
		  		{id: '@id'},
		  		{
			        'update': { method:'PUT' }
		    	}
		    );
		}]);

	app.factory('ReceivedGifts', ['$resource',
		function($resource) {
		  	return $resource(
		  		'api/gifts/:id/received',
		  		{id: '@id'},
		  		{
			        'update': { method:'PUT' }
		    	}
		    );
		}]);


	app.factory('Gift', ['$resource',
		function($resource) {
		  	return $resource(
		  		'gifts/:id',
		  		{id: '@id'},
		  		{
			        'update': { method:'PUT' }
		    	}
		    );
		}]);


	app.run(function($rootScope, $location) {
    	$rootScope.go = function (path) {
	        $location.path(path);
		}

		$rootScope.showError = function(resp, scope) {
			scope.flash_message = [];
			scope.flash_message.push((function() {
				var arr = [];
				for (var name in resp.data.message) {
					arr.push(resp.data.message[name]);
				}

				return arr;
			})());
		}
	});


	app.factory('globals', function(){
		return {
			hostname: function(url){
				return url.match('https?://([^/]*)') ? url.match('https?://([^/]*)')[1] : '';
			}
		};
	});


	var checkLoggedin1 = function($q, $timeout, $http, $location, $rootScope, $window) {
		var deferred = $q.defer();

		$http.get( $rootScope.__base + 'loggedin').success(function(user) {
			if (user !== '0') {
				deferred.resolve();
			}

			else {
				$rootScope.message = 'You need to log in.';
				deferred.reject();
				$window.location.href = $rootScope.__base + 'login';
			}

			return deferred.promise;
		});
	}

	// var app = angular.module("app", ["ngResource"], function($interpolateProvider) {
	//     $interpolateProvider.startSymbol('[[');
	//     $interpolateProvider.endSymbol(']]');
	// });



	// app.value('clientId', 'a12345654321x');

	// app.controller('DemoController', ['clientId', function DemoController(clientId) {
	//   this.clientId = clientId;
	// }]);

	// //$log(Event.query());

	// app.controller('EventController', function($log) {
	// 	// $scope.events = Event.query();
	// 	// this.events = ['a', 'b'];
	// 	this.events = ['a', 'b'];
	// 	$log.info(this);
	// 	$log.info(Event.query());
	// });
})();