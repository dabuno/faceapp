(function() {
	var app = angular.module('controllers', []);

	app.controller('WishlistBook', ['$scope', '$routeParams', '$location', 'WishlistToken', 'globals',
		function($scope, $routeParams, $location, WishlistToken, globals) {
			$scope.wishlist = WishlistToken.get({id: $routeParams.id}, function(res) {
					if (!res || !res.id) {
						$location.path('/');
					}
					if (res.redirect) {
						$location.path('/wishlists/' + res.id);
					}
				});
			$scope.globals = globals;
		}]);

	//single wishlist
	app.controller('WishlistController', ['$scope', '$routeParams', '$timeout', '$http', '$location', 'Wishlist', 'Gift',
		function($scope, $routeParams, $timeout, $http, $location, Wishlist, Gift) {
			$scope.$watch('wishlist_editing', function(newValue, oldValue, scope) {
				if (!newValue) {
					return;
				}
				$timeout(function() {
					angular.element('#wishlist_name').focus();
				});
			});

			//wishlist status
			$scope.editing = $routeParams.id || 0;
			if ($scope.editing)
			{
				//when getting a wishlist, check to see if it has a valid id
				$scope.wishlist = Wishlist.get({id: $routeParams.id}, function(res) {
					if (!res || !res.id) {
						$location.path('/');
					}
				});
			}
			else {
				$scope.wishlist_editing = true;
				$scope.wishlist = new Wishlist();
			}
			$scope.wishlist.gifts = $scope.wishlist.gifts || [];

			//create or update wishlist
			$scope.saveWish = function(wishlist) {
				$scope.wishlist_editing = false;

				if (!$scope.wishlist.name) {
					$scope.wishlist.name = 'wishlist';
				}

				if ($scope.editing) {
					$scope.wishlist.$update();
				}
				else {
					$scope.wishlist.$save(function() {
						$scope.add_gift();
					});
				}
			}


			//fading out button for wishlist when add/edit-ing a gift
			var wishlist_buttons = angular.element('.whislist-buttons').children('button');

			//add gift for a wishlist
			$scope.add_gift = function() {
				$scope.errorName = false;

				$scope.wishlist.gifts.push(new Gift());
				$timeout(function() {
					var gb = angular.element('.jhidden.gift-edit').last().removeClass('jhidden');
					gb.removeClass('jhidden');
				});

				angular.forEach(wishlist_buttons, function(element){
					angular.element(element).addClass('button-fade');
				});
			};

			//edit gift
			$scope.edit = function(gift, index) {
				$scope.errorName = false;
				angular.element('.gift-id-' + index).toggle();
				angular.element('.gift-id-' + index + ' input[name="name"]').focus();
			};

			//cancel add gift form button
			$scope.cancel = function(gift, index) {
				if (!gift.id) {
					$scope.wishlist.gifts.splice(index, 1);
				}
				else {
					angular.element('.gift-id-' + index).hide();
				}

				angular.forEach(wishlist_buttons, function(element){
					angular.element(element).removeClass('button-fade');
				});
			};

			//saving gift and adding new gift form
			$scope.saveAndNew  = function(gift, index) {
				$scope.save(gift, index, function() {
					$scope.add_gift();
				});
			}


			//saving a gift in wishlist route id
			$scope.save = function(gift, index, callback) {
				var gift = new Gift(gift);
				gift.wishlist_id = $routeParams.id || $scope.wishlist.id;

				function success_callback(resp) {
					$scope.wishlist.gifts[index] = resp;
					angular.element('.gift-id-' + index).hide();
					callback = callback || null;
					if (callback) {
						callback();
					}

				};

				function error_callback(res) {
					$scope.errorName = res.data.message.name.join(' ') || false;
				};

				if (gift.id) {
					gift.$update(success_callback, error_callback);
				}
				else {
					gift.$save(success_callback, error_callback);
				}

				//restore buttons for wishlist
				angular.forEach(wishlist_buttons, function(element){
					angular.element(element).removeClass('button-fade');
				});

			}

			//removing a gift from wishlist
			$scope.remove = function(gift, index) {
				var gift = new Gift(gift);
				var promise = gift.$delete();
	  			promise.then(function() {
	  			// 	//update view wo deleted object
	  				$scope.wishlist.gifts.splice(index, 1);
	  			});
	  		};

	  		//clear gift filter
	  		//@todo - not working
	  		$scope.clearFilter = function() {
	  			$scope.wishlist.gifts = '';
	  		};


	  		$scope.share = function() {
	  			// the server should do all the required validation:
	  			// user is allowed to share?
	  			//
	  			// after this, replaces the url in the fb popup.
	  			FB.ui({
	  			  method: 'feed',
	  			  link: $scope.__base_url + '/wish/' + $scope.wishlist.token,
	  			  caption: $scope.wishlist.name + ' wishlist',
	  			});
	  		}

		}]);



	//multiple wishlistS
	app.controller('WishlistsController', ['$scope', 'Wishlist',
		function($scope, Wishlist) {
			$scope.wishlists = Wishlist.query();

	  		$scope.clearFilter = function() {
	  			$scope.searchWishlist = '';
	  		};

	  		$scope.remove = function(Wishlist, idx)
	  		{
	  			var promise = Wishlist.$delete();
	  			promise.then(function() {
	  				//update view wo deleted object
	  				$scope.wishlists.splice(idx, 1);
	  			})
	  		};

		}]);


	app.controller('ReceivedController', ['$scope', 'ReceivedGifts',
		function($scope, ReceivedGifts) {
			$scope.received = ReceivedGifts.query();
		}]);


	app.controller('GiftController', ['$scope', '$routeParams', 'Gift',
		function($scope, $routeParams, Gift) {
	  		$scope.gifts = Gift.query(function(data) {
	  			$scope.gifts.wishlist = [];
	  			$scope.gifts.received = [];
	  			angular.forEach(data, function(value, key){
	  				if (value.status == 'wishlist')
	  				{
	  					$scope.gifts.wishlist.push(value);
	  				}
	  				else if (value.status == 'received')
	  				{
	  					$scope.gifts.received.push(value);
	  				}
	  			});
	  		})

	  		$scope.flash_message = [];
			$scope.getErrors = function() {
				return $scope.flash_message.join('<br />');
			}


	  		$scope.removeGift = function(Gift, idx)
	  		{
	  			var promise = Gift.$delete();
	  			promise.then(function() {
	  				//update view wo deleted object
	  				$scope.gifts.splice(idx, 1);
	  			})
	  		};

	  		$scope.clearFilter = function() {
	  			$scope.searchGift = '';
	  		};

		}]);

	app.controller('GiftAddController', ['$scope', '$routeParams', 'Event', 'Gift',
		function($scope, $routeParams, Event, Gift) {
			$scope.operation = 'Adding';
			$scope.event = Event.get({id: $routeParams.eventId}, function(events) {});

			$scope.gift = new Gift();
			$scope.gift.event_id = $routeParams.eventId;

			$scope.flash_message = [];
			$scope.getErrors = function() {
				return $scope.flash_message.join('<br />');
			}

			$scope.saveGift = function(path) {
				path = path || null;
				$scope.gift.$save(
					function() { //on success
						if (path) {
							$scope.go(path);
						}
					},
					function(resp) {
						$scope.showError(resp, $scope)
				});

			};


		}]);

	app.controller('GiftWishlistController', ['$scope', 'Gift',
		function($scope, Gift) {
			$scope.list_type = 1;

	  		$scope.gifts = Gift.query(function(data) {
	  			$scope.gifts.wishlist = [];
	  			$scope.gifts.received = [];
	  			angular.forEach(data, function(value, key){
	  				if (value.status == 'wishlist')
	  				{
	  					$scope.gifts.wishlist.push(value);
	  				}
	  				else if (value.status == 'received')
	  				{
	  					$scope.gifts.received.push(value);
	  				}
	  			});
	  		});

	  		$scope.removeGift = function(Gift, idx)
	  		{
	  			var promise = Gift.$delete();
	  			promise.then(function() {
	  				//update view wo deleted object
	  				$scope.gifts.splice(idx, 1);
	  			})
	  		};
		}]);

	app.controller('GiftReceivedController', ['$scope',
		function($scope) {
			$scope.list_type = 2;
		}]);


	app.controller('UserController', ['$window', '$scope',
		function($window, $scope) {
			$window.location.href = $scope.__base + "logout";
		}]);
})();