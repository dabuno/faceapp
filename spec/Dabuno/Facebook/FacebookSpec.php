<?php

namespace spec\Dabuno\Facebook;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

use Illuminate\Routing\UrlGenerator;

class FacebookSpec extends ObjectBehavior
{

    function let(UrlGenerator $url)
    {
        $this->beConstructedWith($url);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Dabuno\Facebook\Facebook');
    }

    function it_generates_login_url() {
    	$this->loginURL("test")->shouldStart('<a href=')->shouldContain("test");
    }

    public function getMatchers()
    {
    	return [
    		'start' => function($subject, $value) {
    			return preg_match('@^'.$value.'@', $subject);
    		},

    		'contain' => function($subject, $value)
    		{
    			return preg_match('@'.$value.'@', $subject); 
    		}
    	];
    }

}
