# files_ahead=3
# echo $week
# this_monday=`date --date "last monday" +%d.%m`
# echo $this_monday
# this_sunday=`date --date "this sunday" +%d.%m`
# echo $this_sunday
# this_monday=`date --date "next monday" +%d.%m`
# echo $this_monday
# this_monday=`date --date "next monday" +%d.%m`
# echo $this_monday

DEBUG=${1:-0}

function show(){
	if [ "$DEBUG" -ne "1" ]
	then
		exit
	fi
	echo $1
}


for i in {0..3}; do
	current=`date +%w`
	show "current day: $current"
	if [ $current -eq "0" ]
	then
		this_monday=`date --date "$(($i+1)) monday" +%d.%m`
	else
		this_monday=`date --date "$i monday" +%d.%m`
	fi
	show $this_monday
	show $i
	this_week=`date --date "$i week" +%V`
	show $this_week
	this_sunday=`date --date "$(($i+1)) sunday" +%d.%m`
	show $this_sunday
	filename="tl.W$this_week.$this_monday-$this_sunday.org"
	if [ ! -f "$filename" ]
	then
		touch $filename
	fi

done
