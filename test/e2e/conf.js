// conf.js
exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  // specs: ['spec.js', 'spec2.js']
  specs: ['events.js'],

    // Options to be passed to Jasmine-node.
  jasmineNodeOpts: {
    showColors: true, // Use colors in the command line report.
    isVerbose: true,
    includeStackTrace: true
  },


  // This can be changed via the command line as:
  // --params.login.user 'ngrocks'
  params: {
    login: {
      user: 'protractor-br',
      password: '#ng123#'
    }
  },
  /*
  describe('login page', function() {

  var params = browser.params;

  it('should login successfully', function() {
    element( by.model('username') ).sendKeys( params.login.user );
    element( by.model('password') ).sendKeys( params.login.password );
    element( by.css('[ng-click="login()"]') ).click();
    expect( element(by.binding('username') ).getText() ).toEqual( params.login.user );
  });

});
   */

 // suites: {
 //    homepage: 'tests/e2e/homepage/**/*Spec.js',
 //    search: ['tests/e2e/contact_search/**/*Spec.js']
 //  },
 //  protractor protractor.conf.js --suite homepage


  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    'browserName': 'chrome'
  },

  onPrepare: function() {
  	browser.driver.manage().window().setSize(500, 600);
  }
}