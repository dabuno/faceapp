var hasClass = function (element, cls) {
    return element.getAttribute('class').then(function (classes) {
        return classes.split(' ').indexOf(cls) !== -1;
    });
};



// spec.js
describe('angularjs homepage', function() {
	beforeEach(function() {
			browser.get('http://faceapp.com/im/4');
		});


  	it('should have a title', function() {
    	expect(browser.getTitle()).toEqual('giftapp.io :: Home');
  	});

	it('should delete all events', function() {
		// browser.pause();
		element.all(by.css('[ng-click="removeEvent(event, $index)"]')).each(function(element) {
			element.click();
		});
	});

  	it('should add an event', function() {
  		var event_name = 'test event ' + (+new Date());
  		element(by.css('[title="add event"]')).click();
  		element(by.id('name')).click();
  		element(by.id('name')).sendKeys(event_name);
  		element(by.css('.btn.btn-primary.btn-xs')).click();
  		element(by.css('.fa.fa-search.cp')).click();
  		element(by.model('searchEvent')).sendKeys(event_name);

	    var visible = element.all(by.repeater('event in events')).reduce(function(acc, elem) {
	    	acc = acc || '';
		  return elem.getText().then(function(text) {
		  	if (text && text == event_name)
		    	return acc+text;
		  });
		});

		expect(visible).toEqual(event_name);

  	});
});