var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');
var phpspec = require('gulp-phpspec');
var notify = require('gulp-notify');
var run = require('gulp-run');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var jshint = require('gulp-jshint');
var codecept = require('gulp-codeception');
var gulp = require('gulp');
var ngmin = require('gulp-ngmin');
var concat = require('gulp-concat');


gulp.task('angular', function () {
    gulp.src(['public/assets/js/app.js', 'public/assets/js/controllers.js'])
    	.pipe(ngmin())
    	.pipe(concat("app.js"))   // Combine into 1 file
        // .pipe(ngmin({dynamic: true}))

        .pipe(uglify())
        .pipe(rename({extname: ".min.js"}))
        .pipe(gulp.dest('public/assets/js/dist'))
		.pipe(notify({ message: 'Angular minimized' }));

});




gulp.task('css', function() {
	gulp.src('app/assets/sass/main.scss')
		.pipe(sass())
		.pipe(autoprefixer('last 10 version'))
		.pipe(minifyCSS({keepBreaks:true}))
		.pipe(gulp.dest('public/css/'))
		.pipe(notify({ message: 'Styles task complete' }));
});

gulp.task('compress', function() {
  gulp.src('lib/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('dist'))
});

gulp.task('test', function() {
	gulp.src('spec/**/*.php')
		.pipe(phpspec('', {notify:true}))
		.on('error', notify.onError({
			title: 'Crap',
			message: 'tests failed'
		}))
		.pipe(notify('All done!'))
});

gulp.task('phpspec', function() {
	var options = {debug: true};
	gulp.src('spec/**/*.php')
		.pipe(phpspec('', options))
		.on('error', notify.onError({
			title: "Testing Failed",
			message: "Error(s) occurred during test..."
		}));
});

gulp.task('codecept', function() {
    gulp.src('./app/tests/*.php')
    	.pipe(codecept('', {notify: true}))
    	.on('error', notify.onError({
			title: "Codeception Tests Failed",
			message: "Codeception error(s) occurred during test..."
		}))
		.pipe(notify('Codeception all done!'))
});

gulp.task('watch', function() {
	gulp.watch('app/assets/sass/**/*.scss', ['css']);
	gulp.watch('spec/**/*.php', ['test']);
	gulp.watch('app/Dabuno/**/*.php', ['test']);
	gulp.watch('./tests/**/*.php', ['codecept']);
	gulp.watch('public/assets/js/*.js', ['angular']);
});

gulp.task('scripts', function() {
  return gulp.src('app/assets/scripts/**/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('public/js/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('dist/assets/js'))
    .pipe(notify({ message: 'Scripts task complete' }));
});

gulp.task('default', ['watch']);